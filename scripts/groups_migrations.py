"""Gitlab group migration feature"""

from utils.const import CHUNKSIZE
from utils.shell import print_step, os
from utils.workers import group_worker, subgroup_worker
from utils.find import find_groups

if os() == "Windows":
    from multiprocessing.pool import ThreadPool as Pool
else:
    from multiprocessing import Pool


def groups_migrations(source_gl, dest_gl, **kwargs):
    """
    Group migration function
    """
    overwrite = kwargs.get("overwrite", False)
    requests_groups = [group for group in find_groups(source_gl) if not group.parent_id]
    source_groups_id = [
        group.id
        for group in requests_groups
        if overwrite or not dest_gl.groups.list(search=group.full_name)
    ]
    print_step(len(source_groups_id), "groups")

    with Pool() as group_pool:
        group_pool.map(group_worker, source_groups_id, CHUNKSIZE)

    for top_group_id in source_groups_id:
        source_group = source_gl.groups.get(top_group_id)
        source_subgroups = [
            group.id for group in source_group.descendant_groups.list(all=True)
        ]

        with Pool() as subgroup_pool:
            subgroup_pool.map(subgroup_worker, source_subgroups, CHUNKSIZE)
