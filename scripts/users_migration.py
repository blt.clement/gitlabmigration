"""User migration feature"""

from utils.shell import print_step, os
from utils.workers import user_worker
from utils.const import CHUNKSIZE

if os() == "Windows":
    from multiprocessing.pool import ThreadPool as Pool
else:
    from multiprocessing import Pool


def users_migrations(source_gl, dest_gl, **kwargs):
    """
    Migrate all the source gitlab users to the new gitlab
    """
    overwrite = kwargs.get("overwrite", False)

    source_users_id = [
        user.id
        for user in source_gl.users.list(all=True)
        if overwrite or not dest_gl.users.list(username=user.username)
    ]

    print_step(len(source_users_id), "users")

    with Pool() as pool:
        pool.map(user_worker, source_users_id, CHUNKSIZE)
