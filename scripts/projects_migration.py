"""Gitlab project migration feature"""

from re import search
from utils.const import CHUNKSIZE
from utils.shell import print_step, os, print_update_step
from utils.workers import project_worker, project_update_worker
from utils.find import find_all_projects

if os() == "Windows":
    from multiprocessing.pool import ThreadPool as Pool
else:
    from multiprocessing import Pool


def projects_migration(source_gl, dest_gl, **kwargs):
    """
    Project migration function using multiprocessing
    """
    restrict_group = kwargs.get("restrict_group", "")
    overwrite = kwargs.get("overwrite", False)
    project_limit = kwargs.get("project_limit", None)
    project_name = kwargs.get("project_name", "")

    source_projects = [
        project
        for project in find_all_projects(source_gl)
        if search(restrict_group, project.path_with_namespace)
    ]

    
    source_projects_ids = [
        project.id
        for project in source_projects
        if overwrite
        or not dest_gl.projects.list(
            search_namespaces=True, search=project.path_with_namespace
        )
    ]

    print_step(len(source_projects_ids[0:project_limit]), "projects")

    with Pool() as project_pool:
        project_pool.map(
            project_worker, source_projects_ids[0:project_limit], CHUNKSIZE
        )


def project_info_migration(source_gl, **kwargs):
    """
    Update all the projects
    """
    restrict_group = kwargs.get("restrict_group", "")
    project_name = kwargs.get("project_name", None)


    source_projects_ids = [
        project.id
        for project in find_all_projects(source_gl)
        if search(restrict_group, project.path_with_namespace)
    ]

    print_update_step(len(source_projects_ids))

    with Pool() as update_pool:
        update_pool.map(project_update_worker, source_projects_ids, CHUNKSIZE)
