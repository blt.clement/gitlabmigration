"""Main script to make the migration between the two configured gitlab"""

import configure as conf
from utils.shell import (
    create_tmp,
    delete_tmp,
    clear,
    print_connection,
    init_log,
    continue_script,
)
from utils.connection import connect
from scripts.users_migration import users_migrations
from scripts.groups_migrations import groups_migrations
from scripts.projects_migration import projects_migration, project_info_migration
from utils.update import update_notifications

clear()
create_tmp()
init_log()

source_gl = connect(conf.SOURCE_GL_SERVER, conf.SOURCE_GL_TOKEN)
dest_gl = connect(conf.DEST_GL_SERVER, conf.DEST_GL_TOKEN)
print_connection(conf.SOURCE_GL_SERVER)
print_connection(conf.DEST_GL_SERVER)

"""
continue_script()
users_migrations(source_gl, dest_gl)
continue_script()
groups_migrations(source_gl, dest_gl)

continue_script()
projects_migration(source_gl, dest_gl)
"""
continue_script()
project_info_migration(source_gl,project_name="The Expert V2")

delete_tmp()
