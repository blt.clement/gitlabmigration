# Gitlab Migration Project

Gitlab migration script:

This tool allow you to export (users,groups and projects) from a gitlab instance to another one

This migration script use the python-gitlab api and multiprocessing

Official documentation:

-[Gitlab API Documentation](https://docs.gitlab.com/ee/api/)

-[Python Gitlab API Documentation](https://python-gitlab.readthedocs.io/en/stable/)

## Requirements

See the "requirements.txt" file to install all needed dependencies

To download python : [Download Python](https://www.python.org/download/)

## Configure the migration link

You need to configure a `configure.py` file before launching the script.
The personnal access token has to be created by an admin user (who has all the rigths on groups and projects)

```python
        DEST_GL_SERVER="https://URL_TO_NEW_GITLAB"          # new gitlab url server 
        SOURCE_GL_SERVER="https://URL_TO_OLD_GITLAB"        # old gitlab url server
        DEST_GL_TOKEN="PersonnalAccessTokenOfNewServer"     # Personnal Access Token of new gitlab server
        SOURCE_GL_TOKEN="PersonnalAccessTokenOfOldServer"   # Personnal Access Token of old gitlab server
```

## Launch the script

Be sure to set a maintenance state for your old gitlab as new data will not be transferred

In order to begin the transfer, launch at the root:

Linux :

```bash
    python3 migration.py
```

Windows (PowerShell):

```bash
    py migration.py
```

### Issues

Depending on your projects and gitlab server performances, importing takes time.
Some updates are blocked while the import is not done.

In the `utils/const.py` file you can change 3 values :

```python
    TIMEOUT
    RETRYTIME
    ADMIN
```

`TIMEOUT` -> Controls the authorized time (in second) to block a request

`RETRYTIME` -> Controls the time before a new request

`ADMIN` -> Username of your admin user

If something goes wrong, you can also erase all your data in the new gitlab server with `erase.py` file.
(The quickest way is to restart a new gitlab server)
