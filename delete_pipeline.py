"""Erase the distant gitlab server"""

import configure as conf
from utils.shell import clear, print_connection
from utils.connection import connect

clear()

dest_gl = connect(conf.DEST_GL_SERVER, conf.DEST_GL_TOKEN)
print_connection(conf.DEST_GL_SERVER)

project = dest_gl.projects.get(26)

print(f"Projet: {project.name}\n")

pipelines = project.pipelines.list(get_all=True)

for pipeline in pipelines:
    if project.pipelines.get(pipeline.id).user['username']=='cbouillot':
        pipeline.delete()
