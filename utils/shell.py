"""Shell features depending on the os"""

from platform import system as os
from os import system, path
from datetime import datetime
from sys import exit as new_exit

def clear():
    """
    Clear the shell
    """
    if os() == "Windows":
        system("cls")
    else:
        system("clear")


def create_tmp():
    """
    Create a temporary directory to export/import data
    """
    if os() == "Windows":
        system("md .tmp")
    else:
        system("mkdir -p .tmp")


def delete_file(file_path):
    """
    Delete a file
    """
    if os() == "Windows":
        system(f"del {file_path}")
    else:
        system(f"rm {file_path}")


def delete_tmp():
    """
    delete the temporary directory
    """
    if os() == "Windows":
        system("del -Recurse .tmp")
    else:
        system("rm -r .tmp")


def print_connection(url):
    """
    Print the connection state
    """
    print(f"Connected to {url}")


def print_migration(item):
    """
    Print the migration step
    """
    print(f"    Migrating {item.name} ")


def print_step(number, item):
    """
    Print the migration step
    """
    print(f"\n\n Transferring {number} {item} : \n")

def print_update_step(number):
    """
    Updating project step
    """
    print(f"\n\nUpdating {number} project : \n")

def print_updating(item):
    """
    Print the updating object
    """
    print(f"    Updating {item.name} merge request")


def tmp_path(item):
    """
    Build the path depending on the os
    """
    parent_dir = path.dirname(path.dirname(path.abspath(__file__)))
    dir_path = path.join(parent_dir, ".tmp")

    return path.join(dir_path, f"import_{item.id}.tar.gz")


def init_log():
    """
    Initialize log for the session
    """
    dt_string = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    system(f"echo  '[{dt_string}]' > logs")


def add_log(info):
    """
    Add information to the log file
    """
    system(f"echo {info} >> logs")


def continue_script():
    """
    Wait a user signal to continue or stop the script !
    """
    while True:
        str_input = input("\nDo you want to continue ? [Y/N]")
        if str_input == "Y" or str_input == "y":
            break
        elif str_input == "N" or str_input == "n":
            new_exit(0)
