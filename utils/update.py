"""Updating scripts for gitlab items"""
from gitlab.const import NotificationLevel
from gitlab import GitlabCreateError
from utils.find import find_project, find_group, find_user, find_merge_requests
from utils.shell import add_log


def update_project(dest_gl, src_project):
    """
    Update components of a project
    """
    dest_project = find_project(dest_gl, src_project)
    settings = dest_project.notificationsettings.get()
    settings.level = NotificationLevel.DISABLED
    settings.save()
    update_visibility(src_project, dest_project)
    update_merge_request(dest_gl, src_project, dest_project)


def update_groups(dest_gl, source_group):
    """
    Update components of a group
    """
    dest_group = find_group(dest_gl, source_group)
    settings = dest_group.notificationsettings.get()
    settings.level = NotificationLevel.DISABLED
    settings.save()
    update_members(dest_gl, source_group, dest_group)
    update_visibility(source_group, dest_group)


def update_visibility(src_item, dest_item):
    """
    update the visibility of a gitlab project, group ...
    """
    dest_item.visibility = src_item.visibility
    dest_item.save()


def update_merge_request(dest_gl, src_project, dest_project):
    """
    Update the exported merge request
    """
    src_merge_requests = src_project.mergerequests.list(state="all", all=True)
    for src_merge in src_merge_requests:
        verbose_merge = find_merge_requests(src_project, src_merge.iid)
        dest_merge = find_merge_requests(dest_project, src_merge.iid)

        assignee = verbose_merge.assignee
        assignees = verbose_merge.assignees
        reviewers = verbose_merge.reviewers

        if assignees != []:
            dest_assignee = find_user(dest_gl, assignee["username"])
            assignees_list = []

            for user in assignees:
                assignees_list += [find_user(dest_gl, user["username"]).id]

            dest_merge.assignee_id = dest_assignee.id
            dest_merge.assignees_id = assignees_list

        if reviewers != []:
            reviewer_list = []
            for review in reviewers:
                reviewer_list += [find_user(dest_gl, review["username"]).id]
            dest_merge.reviewer_ids = reviewer_list

        dest_merge.save()


def update_subgroup(dest_gl, source_group):
    """
    Update all the subgroups
    """
    dest_subgroup = find_group(dest_gl, source_group)
    update_members(dest_gl, source_group, dest_subgroup)
    update_visibility(source_group, dest_subgroup)


def update_members(dest_gl, src_item, dest_item):
    """
    Handle members migration
    """
    members = src_item.members.list(get_all=True)
    for member in members:
        try:
            user = dest_gl.users.list(search=member.username)[0]
            dest_item.members.create(
                {"user_id": user.id, "access_level": member.access_level}
            )
        except GitlabCreateError:
            add_log(f"User {member.username} creation problem !")


def update_notifications(dest_gl):
    """
    Update the global notification configuration
    """
    settings = dest_gl.notificationsettings.get()
    settings.level = NotificationLevel.DISABLED
    settings.save()
