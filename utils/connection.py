"""Import requirement to make a gitlab connection"""

from gitlab import Gitlab


def connect(server_url, server_token):
    """
    Connect to a gitlab server and return a gitlab connection
    """
    connection = Gitlab(url=server_url, private_token=server_token, keep_base_url=True)
    connection.auth()

    return connection
