"""Import gitlab features"""

from gitlab import GitlabImportError
from utils.shell import add_log


def import_group(dest_gl, source_group, file_path):
    """
    Import a group to a gitlab server through a connection link
    Return a boolean to indicate the upload status
    """
    try:
        with open(file_path, "rb") as reader:
            dest_gl.groups.import_group(
                reader, path=source_group.path, name=source_group.name, overwrite=True
            )

    except GitlabImportError:
        add_log(f"  {source_group.name} already exist")


def import_project(dest_gl, source_project, file_path):
    """
    Import a project to a gitlab server through a connection link
    Return a boolean to indicate the upload status
    """
    
    with open(file_path, "rb") as reader:
        dest_gl.projects.import_project(
            reader,
            source_project.path,
            source_project.name,
            source_project.namespace["full_path"],
            True,
        )

   

def import_user(dest_gl, source_user):
    """
    Import a user to a gitlab server through a connection link
    """
    user_dict = source_user.__dict__["_attrs"]
    user_dict.update(
        {
            "reset_password": True,
            "public_email": "",
            "admin": user_dict["is_admin"],
            "skip_confirmation": True,
            "bio": user_dict["bio"],
            "can_create_group": user_dict["can_create_group"],
            "external": user_dict["external"],
            "theme_id": user_dict["theme_id"],
            "discord": user_dict["discord"],
            "twitter": user_dict["twitter"],
            "linkedin": user_dict["linkedin"],
            "organization": user_dict["organization"],
            "projects_limit": user_dict["projects_limit"],
            "private_profile": user_dict["private_profile"],
            "note": user_dict["note"],
            "skype": user_dict["skype"]
        }
    )
    if user_dict["identities"]!=[]:
        user_dict.update({
            "provider": user_dict["identities"][0]["provider"],
            "extern_uid": user_dict["identities"][0]["extern_uid"]
            })
        
    dest_user = dest_gl.users.create(user_dict)

    if user_dict["state"] == "blocked":
        dest_user.block()
