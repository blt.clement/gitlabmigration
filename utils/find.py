"""Import gitlab Project object"""

from time import time
from gitlab import GitlabGetError, GitlabListError
from utils.const import TIMEOUT
from utils.shell import add_log


def find_group(dest_gl, source_group):
    """
    Find and return a group by its full path
    """
    init_time = time()
    while True:
        if time() - init_time >= TIMEOUT:
            add_log(f"  Timeout on finding group {source_group.id} ")
            break
        try:
            group_list = dest_gl.groups.list(search=source_group.full_name)
            for group in group_list:
                if group.full_name == source_group.full_name:
                    return group
        except GitlabListError:
            add_log(f"GitlabListError-find group {source_group.id}... retrying")


def find_project(dest_gl, src_project):
    """
    Find and return a project by its full path
    """
    init_time = time()
    while True:
        if time() - init_time >= TIMEOUT:
            add_log(f"  Timeout on finding project {src_project.id} ")
            break

        try:
            possible_project = dest_gl.projects.list(
                search_namespaces=True, search=src_project.path_with_namespace
            )
            for project in possible_project:
                if project.path_with_namespace == src_project.path_with_namespace:
                    return project

        except GitlabListError:
            add_log(f"GitlabListError-find project {src_project.id}... retrying")


def find_user(connection, name):
    """
    Find a user by its name and return it
    """
    init_time = time()
    while True:
        if time() - init_time >= TIMEOUT:
            add_log(f"  Timeout on finding user {name}")
            break
        try:
            return connection.users.list(username=name)[0]
        except GitlabListError:
            add_log(f"GitlabListError-find user {name}... retrying")


def find_merge_requests(project, iid):
    """
    Find a merge request and return it,
    Block until the merge request is imported
    """
    init_time = time()
    while True:
        if time() - init_time >= TIMEOUT:
            add_log(f"  Timeout on finding Merge request {iid}")
            break
        try:
            return project.mergerequests.get(iid)

        except GitlabGetError:
            add_log(f"  Impossible to get {project.name} merge request {iid}")


def find_all_projects(source_gl):
    """
    Request all projects and returns it
    """
    init_time = time()
    while True:
        if time() - init_time >= TIMEOUT:
            add_log("  Timeout on listing all projects")
            break
        try:
            return source_gl.projects.list(all=True)

        except GitlabListError:
            add_log("  Impossible to get all projects ... retrying")

def find_groups(source_gl):
    """
    Request all groups and returns it
    """
    init_time = time()
    while True:
        if time() - init_time >= TIMEOUT:
            add_log("  Timeout on listing all groups")
            break
        try:
            return source_gl.groups.list(all=True)

        except GitlabListError:
            add_log("  Impossible to get all groups ... retrying")
