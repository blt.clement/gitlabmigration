"""Import and Export features """

from utils.imports import import_group, import_project
from utils.export import export
from utils.shell import delete_file


def transfer_project(dest_gl, src_project, file_path):
    """
    Import and export a project using temporary file
    """
    export(src_project, file_path)
    import_project(dest_gl, src_project, file_path)
    delete_file(file_path)


def transfer_group(dest_gl, src_group, file_path):
    """
    Import and export a group using temporary file
    """
    export(src_group, file_path)
    import_group(dest_gl, src_group, file_path)
    delete_file(file_path)
