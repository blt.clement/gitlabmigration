"""Module to define constant"""

TIMEOUT = 3600   # Time (seconds) used to avoid http deadlock
ADMIN = "root"  # Username of the import user, not deleted by 'erase.py' script
CHUNKSIZE = 1   # Chunk size of iterator used by multiproccessing pool to feed each proccess
