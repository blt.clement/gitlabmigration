"""Define all the workers used to migrates gitlab data"""

from utils.transfer import transfer_project, transfer_group
from utils.update import update_project, update_groups, update_subgroup
from utils.shell import tmp_path, print_migration, print_updating
from utils.connection import connect
from utils.imports import import_user
import configure as conf


def project_worker(project_id):
    """
    Project worker, it transfers it and make some update
    """
    source_gl = connect(conf.SOURCE_GL_SERVER, conf.SOURCE_GL_TOKEN)
    dest_gl = connect(conf.DEST_GL_SERVER, conf.DEST_GL_TOKEN)
    source_project = source_gl.projects.get(project_id)

    print_migration(source_project)
    transfer_project(dest_gl, source_project, tmp_path(source_project))


def project_update_worker(project_id):
    """
    Merge request worker, it updates all the merge request of a project
    """
    source_gl = connect(conf.SOURCE_GL_SERVER, conf.SOURCE_GL_TOKEN)
    dest_gl = connect(conf.DEST_GL_SERVER, conf.DEST_GL_TOKEN)

    source_project = source_gl.projects.get(project_id)
    print_updating(source_project)

    update_project(dest_gl, source_project)


def group_worker(group_id):
    """
    Group worker, it transfers it and make some update
    """
    source_gl = connect(conf.SOURCE_GL_SERVER, conf.SOURCE_GL_TOKEN)
    dest_gl = connect(conf.DEST_GL_SERVER, conf.DEST_GL_TOKEN)
    source_group = source_gl.groups.get(group_id)
    print_migration(source_group)

    transfer_group(dest_gl, source_group, tmp_path(source_group))
    update_groups(dest_gl, source_group)


def subgroup_worker(group_id):
    """
    Subgroup worker, it updates subgroups
    """
    source_gl = connect(conf.SOURCE_GL_SERVER, conf.SOURCE_GL_TOKEN)
    dest_gl = connect(conf.DEST_GL_SERVER, conf.DEST_GL_TOKEN)
    real_subgroup = source_gl.groups.get(group_id)
    update_subgroup(dest_gl, real_subgroup)


def user_worker(user_id):
    """
    User worker, it creates all the users
    """
    dest_gl = connect(conf.DEST_GL_SERVER, conf.DEST_GL_TOKEN)
    source_gl = connect(conf.SOURCE_GL_SERVER, conf.SOURCE_GL_TOKEN)
    source_user = source_gl.users.get(user_id)
    print_migration(source_user)
    import_user(dest_gl, source_user)
