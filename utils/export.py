"""Export gitlab features"""

from time import time
from gitlab import GitlabGetError, GitlabCreateError
from requests import exceptions
from utils.shell import add_log
from utils.const import TIMEOUT


def create_export(item):
    """
    Request an export file from a gitlab item
    """
    init_time = time()
    while True:
        if time() - init_time >= TIMEOUT:
            add_log(f"  Exporting creation for project {item.id} failed ")
            return None

        try:
            return item.exports.create()

        except GitlabCreateError:
            add_log(f"  Exporting creation for project {item.id} failed")


def export(item, file_path):
    """
    Export an item and write it locally
    """
    export_item = create_export(item)
    init_time = time()
    while True and export_item is not None:
        if time() - init_time >= TIMEOUT:
            add_log(f"  Exporting failed {file_path} ")
            break

        try:
            with open(file_path, "wb") as writer:
                export_item.download(streamed=True, action=writer.write)
            break

        except (
            ValueError,
            GitlabGetError,
            exceptions.ConnectionError,
            exceptions.ConnectTimeout,
        ):
            add_log(f"  waiting export {file_path}")
