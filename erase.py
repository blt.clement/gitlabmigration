"""Erase the distant gitlab server"""

from time import sleep
import configure as conf
from utils.const import ADMIN
from utils.shell import clear, print_connection
from utils.connection import connect

clear()

dest_gl = connect(conf.DEST_GL_SERVER, conf.DEST_GL_TOKEN)

print_connection(conf.DEST_GL_SERVER)

projects = dest_gl.projects.list(all=True)

print("Erasing all projects")
for project in projects:
    dest_gl.projects.delete(project.id)

while dest_gl.projects.list(get_all=False) != []:
    sleep(3)

groups = dest_gl.groups.list(all=True)

print("Erasing groups")
for group in groups:
    dest_gl.groups.delete(group.id)

while dest_gl.groups.list(all=False) != []:
    sleep(3)


users = dest_gl.users.list(all=True)

print("Erasing users")
for user in users:
    if user.username != ADMIN:
        dest_gl.users.delete(user.id)
